package com.galvanize;

import java.math.BigDecimal;
import java.util.ArrayList;

public interface HousingOrder {

    void addItem(CustomerOrder item);

    ArrayList<CustomerOrder> getItems();

    BigDecimal getTotal();
}
