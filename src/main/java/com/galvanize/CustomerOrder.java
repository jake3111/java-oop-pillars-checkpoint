package com.galvanize;

import java.math.BigDecimal;
import java.math.BigInteger;

public abstract class CustomerOrder {
    private long id;

    public CustomerOrder() {
        this.id = 0L;
    }

    public CustomerOrder(long id) {
        this.id = id;
    }

    public long getId()
    {
        return id;
    }

    public abstract BigDecimal totalPrice();
}
