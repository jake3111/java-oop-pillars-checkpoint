package com.galvanize;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;

public class Order implements HousingOrder{

    private ArrayList<CustomerOrder> items = new ArrayList<>();
    private BigDecimal total = new BigDecimal("0.00");

    public void addItem(CustomerOrder item) {
        items.add(item);
        total = total.add(item.totalPrice());
    }

    public ArrayList<CustomerOrder> getItems() {
        return items;
    }

    public BigDecimal getTotal() {
        return total;
    }
}
