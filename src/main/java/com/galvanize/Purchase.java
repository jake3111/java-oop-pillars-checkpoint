package com.galvanize;

import java.math.BigDecimal;

public class Purchase extends CustomerOrder{

    private BigDecimal price;
    private String productName;

    public Purchase(String productName, BigDecimal price) {
        this.productName = productName;
        this.price = price;
    }

    public BigDecimal getPrice() {
        return price;
    }

    @Override
    public String toString() {
        return "Purchase{" +
                "price=" + price +
                ", productName='" + productName + '\'' +
                '}';
    }

    public BigDecimal totalPrice(){
        return getPrice();
    }
}
